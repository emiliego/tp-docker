FROM debian:stable-slim
RUN apt-get update && \
    apt-get install -y apache2 && \
    rm -r /var/lib/apt/lists/*
RUN systemctl start apache2.service
EXPOSE 80
EXPOSE 443
