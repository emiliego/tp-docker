FROM debian:testing-slim
RUN apt-get update && \
    apt-get install -y git npm && \
    git clone https://github.com/jponcy/fake-game-api.git && \
    rm -r /var/lib/apt/lists/*
WORKDIR /fake-game-api
RUN npm i -g npm@latest && \
    npm install
CMD npm run start:merged -- --host 0.0.0.0