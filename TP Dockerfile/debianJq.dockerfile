FROM my-deb:node
RUN apt-get update && \
    apt-get install -y jq && \
    rm -r /var/lib/apt/lists/*
# CMD jq '.[]'