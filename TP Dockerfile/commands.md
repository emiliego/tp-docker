Liste des lignes de commande :

Question 1 :

Dockerfile (debianCurl.dockerfile) :
FROM debian:stable-slim
RUN apt-get update && \
    apt-get install -y curl && \
    rm -r /var/lib/apt/lists/*

$ docker build -t my-deb:curl -f debianCurl.dockerfile .

============================================
Question 2  :

Dockerfile (debianCurl2.dockerfile) :
FROM debian:stable-slim
RUN apt-get update && \
    apt-get install -y curl && \
    rm -r /var/lib/apt/lists/*
ENTRYPOINT [ "curl", "https://http.cat/102" ]

$ docker build -t my-deb:curl -f debianCurl2.dockerfile .
$ docker run --rm my-deb:curl

=============================================
Question 3 :

Dockerfile (debianNode.dockerfile) :
FROM debian:testing-slim
RUN apt-get update && \
    apt-get install -y git npm && \
    git clone https://github.com/jponcy/fake-game-api.git && \
    rm -r /var/lib/apt/lists/*
WORKDIR /fake-game-api
RUN npm i -g npm@latest && \
    npm install
CMD npm run start:merged -- --host 0.0.0.0

$ docker build -t my-deb:node -f debianNode.dockerfile .
$ docker run --rm -p 8087:3000 my-deb:node

==============================================
Question 4 :

Dockerfile (debianJq.dockerfile) : 
FROM my-deb:node
RUN apt-get update && \
    apt-get install -y jq && \
    rm -r /var/lib/apt/lists/*

$ docker build -t debian:jq -f debianJq.dockerfile .
$ docker run --rm -p 8095:3000 debian:jq

==============================================
Question 5 :

Dockerfile (apache.dockerfile) :
FROM httpd:alpine
COPY iia_2021_docker-master/dockerfile/mini-site/ /usr/local/apache2/htdocs/

$ docker build -t apache:git -f apache.dockerfile .
$ docker run --rm -p 8091:80 apache:git
